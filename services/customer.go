// Copyright © 2017 Yo Mangan. All rights reserved.
package services

import (
	"sync"

	"yo-mangan/app"
	"yo-mangan/ioc"
	"yo-mangan/model"
	"yo-mangan/requests"
)

type Customer struct {
	customer  *model.Customer
	customers *model.Customers
	request   *requests.Customer
}

func (c *Customer) Create(request interface{}) (interface{}, error) {
	if err := c.Init(request); err != nil { return nil, err }
	return c.customer, ioc.MySQLDatabase().Transact(app.DB, func() error {
		return app.DB.Create(&c.customer).Error
	})
}

func (c *Customer) Find(fields map[string]interface{}) interface{} {
	customer := &model.Customer{}
	if err := app.DB.First(customer, fields).Error; err != nil || customer.ID == 0 { return nil }
	return customer
}

func (c *Customer) Init(value interface{}) (err error) {
	c.request = value.(*requests.Customer)
	c.customer = model.NewCustomer(c.request.Name, c.request.Address, c.request.Phone, c.request.Email, c.request.UserName, c.request.Password, ioc.Handler().Date(c.request.BirthDate), model.Gender(c.request.Gender))
	return
}

func (c *Customer) Read(fields interface{}) (interface{}, error) {
	return c.customers, ioc.MySQLDatabase().ReadData(app.DB, func() error { return nil }, &c.customers, false, fields)
}

func (c *Customer) Update(id int, request interface{}) (interface{}, error) {
	if err := c.Init(request); err != nil { return nil, err }
	return c.customer, ioc.MySQLDatabase().Transact(app.DB, func() error {
		return app.DB.Model(&c.customer).Updates(c.customer.Map(id)).Error
	})
}

func NewCustomer() *Customer {
	var (
		once sync.Once
		customer *Customer
	)
	once.Do(func() { customer = &Customer{customers: &model.Customers{}} })
	return customer
}