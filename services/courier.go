// Copyright © 2017 Yo Mangan. All rights reserved.
package services

import (
	"sync"

	"yo-mangan/app"
	"yo-mangan/ioc"
	"yo-mangan/model"
	"yo-mangan/requests"
)

type Courier struct {
	courier  *model.Courier
	couriers *model.Couriers
	request   *requests.Courier
}

func (c *Courier) Create(request interface{}) (interface{}, error) {
	if err := c.Init(request); err != nil { return nil, err }
	return c.courier, ioc.MySQLDatabase().Transact(app.DB, func() error {
		return app.DB.Create(&c.courier).Error
	})
}

func (c *Courier) Find(fields map[string]interface{}) interface{} {
	courier := &model.Courier{}
	if err := app.DB.First(courier, fields).Error; err != nil || courier.ID == 0 { return nil }
	return courier
}

func (c *Courier) Init(value interface{}) (err error) {
	c.request = value.(*requests.Courier)
	c.courier = model.NewCourier(c.request.Name, c.request.Address, c.request.Phone, c.request.Email, c.request.UserName, c.request.Password, ioc.Handler().Date(c.request.BirthDate), model.Gender(c.request.Gender))
	return
}

func (c *Courier) Read(fields interface{}) (interface{}, error) {
	return c.couriers, ioc.MySQLDatabase().ReadData(app.DB, func() error { return nil }, &c.couriers, false, fields)
}

func (c *Courier) Update(id int, request interface{}) (interface{}, error) {
	if err := c.Init(request); err != nil { return nil, err }
	return c.courier, ioc.MySQLDatabase().Transact(app.DB, func() error {
		return app.DB.Model(&c.courier).Updates(c.courier.Map(id)).Error
	})
}

func NewCourier() *Courier {
	var (
		once sync.Once
		courier *Courier
	)
	once.Do(func() { courier = &Courier{couriers: &model.Couriers{}} })
	return courier
}