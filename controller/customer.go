// Copyright © 2017 Yo Mangan. All rights reserved.
package controller

import (
	"sync"

	"yo-mangan/ioc"
	"yo-mangan/requests"
	"yo-mangan/responses"
	"yo-mangan/services"

	"github.com/kataras/iris"
)

type Customer struct {
	request *requests.Customer
	service *services.Customer
}

var customer *Customer

func CreateCustomer(ctx iris.Context) {
	ioc.Handler().Exec(iris.MethodPost, "Customer created", customer.request, &responses.Response{}, func(interface{}) (interface{}, error) {
		return ioc.Module.Inject(customer.service).Create(customer.request)
	}, ctx)
}

func DeleteCustomer(ctx iris.Context) {
	ctx.Next()
}

func ReadCustomers(ctx iris.Context) {
	ioc.Handler().Exec(iris.MethodGet, "Get customers", nil, &responses.Customers{}, func(fields interface{}) (interface{}, error) {
		return ioc.Module.Inject(customer.service).Read(fields)
	}, ctx)
}

func UpdateCustomer(ctx iris.Context) {
	ioc.Handler().Exec(iris.MethodPut, "Customer updated", customer.request, &responses.Response{}, func(interface{}) (interface{}, error) {
		return ioc.Module.Inject(customer.service).Update(ioc.Handler().PrimaryKey(ctx), customer.request)
	}, ctx)
}

func init() {
	var once sync.Once
	once.Do(func() { customer = &Customer{&requests.Customer{}, services.NewCustomer()} })
}