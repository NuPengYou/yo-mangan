// Copyright © 2017 Yo Mangan. All rights reserved.
package controller

import (
	"sync"

	"yo-mangan/ioc"
	"yo-mangan/requests"
	"yo-mangan/responses"
	"yo-mangan/services"

	"github.com/kataras/iris"
)

type Courier struct {
	request *requests.Courier
	service *services.Courier
}

var courier *Courier

func CreateCourier(ctx iris.Context) {
	ioc.Handler().Exec(iris.MethodPost, "Courier created", courier.request, &responses.Response{}, func(interface{}) (interface{}, error) {
		return ioc.Module.Inject(courier.service).Create(courier.request)
	}, ctx)
}

func DeleteCourier(ctx iris.Context) {
	ctx.Next()
}

func ReadCouriers(ctx iris.Context) {
	ioc.Handler().Exec(iris.MethodGet, "Get couriers", nil, &responses.Couriers{}, func(fields interface{}) (interface{}, error) {
		return ioc.Module.Inject(courier.service).Read(fields)
	}, ctx)
}

func UpdateCourier(ctx iris.Context) {
	ioc.Handler().Exec(iris.MethodPut, "Courier updated", courier.request, &responses.Response{}, func(interface{}) (interface{}, error) {
		return ioc.Module.Inject(courier.service).Update(ioc.Handler().PrimaryKey(ctx), courier.request)
	}, ctx)
}

func init() {
	var once sync.Once
	once.Do(func() { courier = &Courier{&requests.Courier{}, services.NewCourier()} })
}
