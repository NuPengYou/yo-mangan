// Copyright © 2017 Yo Mangan. All rights reserved.
package requests

type OrderFood struct {
	OrderID  int     `json:"order_id"`
	FoodID   int     `json:"food_id"`
	Quantity int     `json:"quantity"`
	Price 	 float64 `json:"price"`
	Total 	 float64 `json:"total"`
}