// Copyright © 2017 Yo Mangan. All rights reserved.
package requests

type Order struct {
	Invoice	   string  `json:"invoice"`
	CourierID  int     `json:"courier_id"`
	CustomerID int     `json:"customer_id"`
	Total 	   float64 `json:"total"`
}