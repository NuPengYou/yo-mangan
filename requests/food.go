// Copyright © 2017 Yo Mangan. All rights reserved.
package requests

type Food struct {
	Name  	     string  `json:"name"`
	Price 	     float64 `json:"price"`
	RestaurantID int     `json:"restaurant_id"`
}