// Copyright © 2017 Yo Mangan. All rights reserved.
package requests

type Restaurant struct {
	Name  	string `json:"name"`
	Address string `json:"address"`
}
