// Copyright © 2017 Yo Mangan. All rights reserved.
package requests

type Courier struct {
	Name  	  string `json:"name"`
	Address	  string `json:"address"`
	BirthDate string `json:"birth_date"`
	Gender	  string `json:"gender"`
	Phone	  string `json:"phone"`
	Email 	  string `json:"email"`
	UserName  string `json:"user_name"`
	Password  string `json:"password"`
}