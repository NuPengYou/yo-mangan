// Copyright © 2017 Yo Mangan. All rights reserved.
package router

import (
	"time"

	"yo-mangan/controller"

	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
)

var App *iris.Application

func init() {
	App = iris.New()
	App.WrapRouter(cors.WrapNext(cors.Options{AllowedOrigins: []string{"*"}, AllowCredentials: true}))
	App.Logger().SetLevel("debug")
	group := func(path string, handlers ...func(iris.Context)) {
		v := App.Party("/api/v1", func(ctx iris.Context) { ctx.Next() }).Party(path)
		for index, handler := range handlers {
			switch index {
			case 1:
				v.Delete("/:id", handler)
			case 2:
				v.Get("/", iris.Cache(4 * time.Second), handler)
			case 3:
				v.Post("/", handler)
			case 4:
				v.Put("/:id", handler)
			default:
				v.Any("", iris.Cache(4 * time.Second), handler)
			}
		}
	}
	group("/customers", controller.ReadCustomers, controller.DeleteCustomer, controller.ReadCustomers, controller.CreateCustomer, controller.UpdateCustomer)
	group("/couriers", controller.ReadCouriers, controller.DeleteCourier, controller.ReadCouriers, controller.CreateCourier, controller.UpdateCourier)
	//Router.Get("/authorize", cache(controllers.Authorize))
	//Router.Post("/login", cache(controllers.Login))
}