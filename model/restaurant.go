// Copyright © 2017 Yo Mangan. All rights reserved.
package model

import (
	"sync"
	"time"
)

type (
	Restaurant struct {
		ID	  	  uint       `sql:"auto_increment;primary_key"`
		Name  	  string     `sql:"type:varchar(255);not null"`
		Address   string     `sql:"type:varchar(255);not null"`
		CreatedAt time.Time  `sql:"default:current_timestamp"`
		UpdatedAt time.Time  `sql:"default:current_timestamp on update current_timestamp"`
		DeletedAt *time.Time `sql:"_"`
	}
	Restaurants []*Restaurant
)

func (r *Restaurant) Map(id int) interface{} {
	r.ID = uint(id)
	return map[string]interface{}{"name": r.Name, "address": r.Address}
}

func (r *Restaurant) TableName() string { return "restaurants" }

func NewRestaurant(name, address string) *Restaurant {
	var (
		once sync.Once
		restaurant *Restaurant
	)
	once.Do(func() { restaurant = &Restaurant{Name: name, Address: address} })
	return restaurant
}