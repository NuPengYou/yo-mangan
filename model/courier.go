// Copyright © 2017 Yo Mangan. All rights reserved.
package model

import (
	"encoding/json"
	"sync"
	"time"
)

type (
	Courier struct {
		ID	  	  uint       `sql:"auto_increment;primary_key"`
		Name 	  string     `sql:"type:varchar(255);not null"`
		Address	  string     `sql:"type:text;not null"`
		BirthDate time.Time  `sql:"type:date;not null"`
		Gender    Gender     `sql:"type:enum('female','male');not null;default:'female'"`
		Phone	  string     `sql:"type:varchar(255);not null"`
		Email 	  string	  `sql:"type:varchar(119);not null"`
		UserName  string     `sql:"type:varchar(85);not null"`
		Password  string     `sql:"type:varchar(85);not null"`
		CreatedAt time.Time  `sql:"default:current_timestamp"`
		UpdatedAt time.Time  `sql:"default:current_timestamp on update current_timestamp"`
		DeletedAt *time.Time `sql:"_"`
	}
	Couriers []*Courier
)

func (c *Courier) auxiliary() (interface{}) {
	type Alias Courier
	return &struct {BirthDate string `json:"birth_date"`; UserName string `json:"user_name"`; *Alias}{c.BirthDate.Format("2006 January 02"), c.UserName, (*Alias)(c)}
}

func (c *Courier) Map(id int) interface{} {
	c.ID = uint(id)
	return map[string]interface{}{"name": c.Name, "address": c.Address, "birth_date": c.BirthDate, "gender": c.Gender, "phone": c.Phone, "email": c.Email, "user_name": c.UserName, "password": c.Password}
}

func (c *Courier) MarshalJSON() ([]byte, error) { return json.Marshal(c.auxiliary()) }

func (c *Courier) TableName() string { return "couriers" }

func (c *Courier) UnmarshalJSON(data []byte) error { return json.Unmarshal(data, c.auxiliary()) }

func NewCourier(name, address, phone, email, userName, password string, birth_date time.Time, gender Gender) *Courier {
	var (
		once sync.Once
		courier *Courier
	)
	once.Do(func() { courier = &Courier{Name: name, Address: address, BirthDate: birth_date, Gender: gender, Phone: phone, Email: email, UserName: userName, Password: password} })
	return courier
}