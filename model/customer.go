// Copyright © 2017 Yo Mangan. All rights reserved.
package model

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"sync"
	"time"
)

const (
	Female Gender = "female"
	Male          = "male"
)

type (
	Gender string
	Customer struct {
		ID	  	  uint       `sql:"auto_increment;primary_key"`
		Name 	  string     `sql:"type:varchar(255);not null"`
		Address	  string     `sql:"type:text;not null"`
		BirthDate time.Time  `sql:"type:date;not null"`
		Gender    Gender     `sql:"type:enum('female','male');not null;default:'female'"`
		Phone	  string     `sql:"type:varchar(255);not null"`
		Email 	  string	 `sql:"type:varchar(119);not null"`
		UserName  string     `sql:"type:varchar(85);not null"`
		Password  string     `sql:"type:varchar(85);not null"`
		CreatedAt time.Time  `sql:"default:current_timestamp"`
		UpdatedAt time.Time  `sql:"default:current_timestamp on update current_timestamp"`
		DeletedAt *time.Time `sql:"_"`
	}
	Customers []*Customer
)

func (g *Gender) Scan(src interface{}) error {
	if b, ok := src.([]byte); ok {
		*g = Gender(string(b))
		return nil
	}
	return fmt.Errorf("Failed to scan %T, value isn't []byte", src)
}

func (g Gender) Value() (driver.Value, error) { return string(g), nil }

func (c *Customer) auxiliary() (interface{}) {
	type Alias Customer
	return &struct {BirthDate string `json:"birth_date"`; UserName string `json:"user_name"`; *Alias}{c.BirthDate.Format("2006 January 02"), c.UserName, (*Alias)(c)}
}

func (c *Customer) Map(id int) interface{} {
	c.ID = uint(id)
	return map[string]interface{}{"name": c.Name, "address": c.Address, "birth_date": c.BirthDate, "gender": c.Gender, "phone": c.Phone, "email": c.Email, "user_name": c.UserName, "password": c.Password}
}

func (c *Customer) MarshalJSON() ([]byte, error) { return json.Marshal(c.auxiliary()) }

func (c *Customer) TableName() string { return "customers" }

func (c *Customer) UnmarshalJSON(data []byte) error { return json.Unmarshal(data, c.auxiliary()) }

func NewCustomer(name, address, phone, email, userName, password string, birth_date time.Time, gender Gender) *Customer {
	var (
		once sync.Once
		customer *Customer
	)
	once.Do(func() { customer = &Customer{Name: name, Address: address, BirthDate: birth_date, Gender: gender, Phone: phone, Email: email, UserName: userName, Password: password} })
	return customer
}