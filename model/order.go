// Copyright © 2017 Yo Mangan. All rights reserved.
package model

import (
	"sync"
	"time"
)

type (
	Order struct {
		ID	  	   uint       `sql:"auto_increment;primary_key"`
		Invoice	   string     `sql:"type:varchar(255);not null;unique;index"`
		Courier    Courier    `gorm:"ForeignKey:CourierID;save_associations:false"`
		CourierID  uint       `sql:"type:int(10)unsigned;not null;index"`
		Customer   Customer   `gorm:"ForeignKey:CustomerID;save_associations:false"`
		CustomerID uint       `sql:"type:int(10)unsigned;not null;index"`
		Total 	   float64    `sql:"type:decimal(9,0);not null;default:0.00"`
		CreatedAt  time.Time  `sql:"default:current_timestamp"`
		UpdatedAt  time.Time  `sql:"default:current_timestamp on update current_timestamp"`
		DeletedAt  *time.Time `sql:"_"`
	}
	Orders []*Order
)

func (o *Order) Map(id int) interface{} {
	o.ID = uint(id)
	return map[string]interface{}{"invoice": o.Invoice, "courier_id": o.CourierID, "customer_id": o.CustomerID, "total": o.Total}
}

func (o *Order) TableName() string { return "orders" }

func NewOrder(invoice string, courier Courier, customer Customer, total float64) *Order {
	var (
		once sync.Once
		order *Order
	)
	once.Do(func() { order = &Order{Invoice: invoice, Courier: courier, CourierID: courier.ID, Customer: customer, CustomerID: customer.ID, Total: total} })
	return order
}