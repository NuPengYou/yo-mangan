// Copyright © 2017 Yo Mangan. All rights reserved.
package model

import (
	"sync"
	"time"
)

type (
	OrderFood struct {
		ID	  	  uint       `sql:"auto_increment;primary_key"`
		Order     Order      `gorm:"ForeignKey:OrderID;save_associations:false"`
		OrderID   uint       `sql:"type:int(10)unsigned;not null;index"`
		Food   	  Food       `gorm:"ForeignKey:FoodID;save_associations:false"`
		FoodID 	  uint       `sql:"type:int(10)unsigned;not null;index"`
		Quantity  int        `sql:"type:tinyint(4);not null"`
		Price     float64    `sql:"type:double;not null"`
		Total     float64    `sql:"type:double;not null"`
		CreatedAt time.Time  `sql:"default:current_timestamp"`
		UpdatedAt time.Time  `sql:"default:current_timestamp on update current_timestamp"`
		DeletedAt *time.Time `sql:"_"`
	}
	OrderFoods []*OrderFood
)

func (of *OrderFood) Map(id int) interface{} {
	of.ID = uint(id)
	return map[string]interface{}{"order_id": of.OrderID, "food_id": of.FoodID, "quantity": of.Quantity, "price": of.Price, "total": of.Total}
}

func (of *OrderFood) TableName() string { return "order_food" }

func NewOrderFood(order Order, food Food, quantity int, price, total float64) *OrderFood {
	var (
		once sync.Once
		orderFood *OrderFood
	)
	once.Do(func() { orderFood = &OrderFood{Order: order, OrderID: order.ID, Food: food, FoodID: food.ID, Quantity: quantity, Price: price, Total: total} })
	return orderFood
}