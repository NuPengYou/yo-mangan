// Copyright © 2017 Yo Mangan. All rights reserved.
package model

import (
	"sync"
	"time"
)

type (
	Food struct {
		ID	  	  	 uint       `sql:"auto_increment;primary_key"`
		Name  	  	 string     `sql:"type:varchar(255);not null"`
		Price     	 float64    `sql:"type:double;not null"`
		Restaurant   Restaurant `gorm:"ForeignKey:RestaurantID;save_associations:false"`
		RestaurantID uint       `sql:"type:int(10)unsigned;not null;index"`
		CreatedAt 	 time.Time  `sql:"default:current_timestamp"`
		UpdatedAt 	 time.Time  `sql:"default:current_timestamp on update current_timestamp"`
		DeletedAt 	 *time.Time `sql:"_"`
	}
	Foods []*Food
)

func (f *Food) Map(id int) interface{} {
	f.ID = uint(id)
	return map[string]interface{}{"name": f.Name, "price": f.Price, "restaurant_id": f.RestaurantID}
}

func (f *Food) TableName() string { return "foods" }

func NewFood(name string, price float64, restaurant Restaurant) *Food {
	var (
		once sync.Once
		food *Food
	)
	once.Do(func() { food = &Food{Name: name, Price: price, Restaurant: restaurant, RestaurantID: restaurant.ID} })
	return food
}