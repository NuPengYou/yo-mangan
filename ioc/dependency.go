// Copyright © 2017 Yo Mangan. All rights reserved.
package ioc

type (
	Administrator interface {
		Create(request interface{}) (interface{}, error)
		Read(fields interface{}) (interface{}, error)
		Update(id int, request interface{}) (interface{}, error)
	}
	Database func() Administrator
	Container struct {
		Database
	}
)

var Module = &Container{}

func (c *Container) Inject(i interface{}) Administrator {
	if t, ok := i.(Administrator); ok {
		c.Database = func() Administrator { return t }
	}
	return c.Database()
}