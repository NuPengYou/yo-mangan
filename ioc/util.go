package ioc

import (
	"sync"
	"time"

	"yo-mangan/helper"

	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
)

type (
	Composer interface {}
	Configurator interface {
		// wrapper function use to read .json file, return an error so that can be handled in main package.
		// name is configuration file name(include path with subdirectory)
		// value is output value that contain extracted data
		Read(name string, value interface{}) error
	}
	Configure func() Configurator
	Cryptographer interface {
		CompareHash(password, hash string) bool
		Eksblowfish(password string, cost int) (string, error)
	}
	Encryption func() Cryptographer
	DBMS interface {
		ReadData(db *gorm.DB, f func() error, out interface{}, expr bool, params ...interface{}) error
		Register() (db *gorm.DB, err error)
		Transact(db *gorm.DB, f func() error) error
	}
	ODBC func() DBMS
	SecureShell interface {
		Connect() *gorm.DB
	}
	AppLayer func() SecureShell
	Wrapper interface {
		Date(value string) time.Time
		Error(format string, args ...interface{}) error
		// wrapper function use in controller package, return an error so that can be handled from router.
		// method is value of HTTP method(GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE and CONNECT)
		// text is message for HTTP response
		// req is HTTP body request
		// res is result of HTTP response
		// f is closure function for embedded process
		// ctx is context(iris.Context) to get HTTP request also return HTTP response
		Exec(method, text string, req, res interface{}, f helper.Function, ctx iris.Context) error
		// wrapper function for get data from external API.
		// url is target external API url
		HTTPRequest(url string) (interface{}, error)
		// wrapper function that marshal then directly unmarshal struct data to JSON format.
		// i is input for any data that will be encoded into JSON
		// v is value that hold the JSON result
		JSON(i, v interface{}) error
		// wrapper function that return primary key(id) from API, used in update data process.
		// ctx is context(iris.Context) to get URL parameter
		PrimaryKey(ctx iris.Context) int
		// wrapper function that trace function at runtime.
		// sep is separator of routine path(e.g. path.struct.name.routine), it will split if set with "."
		// skip is count of how many caller that you want to traces
		Trace(sep string, skip int) []string
	}
	Subroutine func() Wrapper
	Timestamp time.Time
)

var (
	Encoder Encryption
	Handler Subroutine
	MySQLDatabase ODBC
)

func init() {
	var once sync.Once
	database, encoder, handler := make(chan ODBC, 1), make(chan Encryption, 1), make(chan Subroutine, 1)
	once.Do(func() {
		go func(db chan<- ODBC) {
			db <- func() DBMS { return &helper.MySQL{} }
			defer close(db)
		}(database)
		go func(cryptographer chan<- Encryption) {
			cryptographer <- func() Cryptographer { return &helper.Cipher{} }
			defer close(cryptographer)
		}(encoder)
		go func(wrapper chan<- Subroutine) {
			wrapper <- func() Wrapper { return &helper.FunctionWrapper{} }
			defer close(wrapper)
		}(handler)
		Encoder, Handler, MySQLDatabase = <-encoder, <-handler, <-database
	})
}