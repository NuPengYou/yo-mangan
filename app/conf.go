// Copyright © 2017 Yo Mangan. All rights reserved.
package app

import (
	"fmt"

	"yo-mangan/helper"
	"yo-mangan/ioc"
	"yo-mangan/model"

	"github.com/jinzhu/gorm"

	_ "github.com/go-sql-driver/mysql"
)

type (
	Auth struct {
		Token struct {
			Expiry int    `json:"expiry"`
			Secret string `json:"secret"`
			Type   string `json:"type"`
		} `json:"token"`
	}
	Conf struct {
		Auth 	 Auth     `json:"auth"`
		Database Database `json:"database"`
		Network  Network  `json:"network"`
		Server   Server   `json:"server"`
	}
	Database struct {
		MySQL struct {
			Host     string `json:"host"`
			Username string `json:"username"`
			Password string `json:"password"`
			Database string `json:"database"`
			Port     int    `json:"port"`
		} `json:"mysql"`
	}
	Network struct {
		SSH struct {
			Host     string `json:"host"`
			Username string `json:"username"`
			Password string `json:"password"`
			Port     int    `json:"port"`
		} `json:"ssh"`
	}
	Server struct {
		Backend struct {
			Host string `json:"host"`
			Port int    `json:"port,string"`
		} `json:"backend"`
	}
)

var (
	Cfg Conf
	DB *gorm.DB
)

func (s *Server) Addr() string { return fmt.Sprintf("%s:%d", s.Backend.Host, s.Backend.Port) }

func init() {
	configurator := func() ioc.Configurator { return &helper.Config{} }
	configurator().Read("auth.json", &Cfg.Auth)
	configurator().Read("databases/yo-mangan.json", &Cfg.Database)
	DB = func() ioc.SecureShell { return &helper.SSH{&helper.MySQL{OverSSH: false, Connection: &helper.ConnectionString{Cfg.Database.MySQL.Host, Cfg.Database.MySQL.Username, Cfg.Database.MySQL.Password, Cfg.Database.MySQL.Database, Cfg.Database.MySQL.Port}}, Cfg.Network.SSH.Host, Cfg.Network.SSH.Username, Cfg.Network.SSH.Password, Cfg.Network.SSH.Port} }().Connect()
	DB.AutoMigrate(&model.Courier{}, &model.Customer{}, &model.Food{}, &model.Order{}, &model.OrderFood{}, &model.Restaurant{})
	configurator().Read("ssh.json", &Cfg.Network)
	configurator().Read("server.json", &Cfg.Server)
}