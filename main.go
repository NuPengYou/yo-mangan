package main

import (
	"yo-mangan/app"
	"yo-mangan/router"

	"github.com/kataras/iris"
)

func init() {
	println(`
	                                        @@@    @@         @@
	                                 @@     &@#    @@.   @        @@
	                              @       @            @   @@@ @    @
	                           @        @@           @@ @    @@@@   @
	                        @           @          @@@@.@@      @
	                     @              ,          @@@@@ @        @@
	                  @  @@     @@      @                @          @
	                @  @                 @              @            @
	                            @@@% @    @           @               @
	             @   @         @@@@ @@       @@@@@@@                   @
	             @   @          @@@@  , &@@@@                           @
	        @@   @   @               % @@@@@@    %@                     @
	       @          @             @  @@@        @                      @
	      @@   @@      @@         @@ @       (@@@                         @
	       @   @@         .@@@@      @    @  @   @                         @
	       @                              @   @                             @@
	         @@@ @                         @   @@                            @@
	             @                                                            @@@@@@
	              @                                                            @    @@
	               @                                                           @@@  @
	                                                                            @
	                                                                            @
	                 @                                        %*                @@
	                                                        @     @              @
	                    @                                         @@             @
	                     @                                @     @@               @
	                      @@                                                     @
	                        @                                                    @
	                                                                             @
	                         @                                                   @
	                         @@                                                  @
	                          @                                                  @
	                          @@                                                 @
	                           @                                                 @
	                           @                                                @
	                           @                                                @
	                           @                                                @
	                           @                                               @
	                           @                                              @    @
	                           @                                             @     @@
	                           @                                            @   @  @
	                            (                                         @
	                           @                                       @
	                            @@@                                @@
	                                                           @@
	                                  @  ,@@              @@@
	                                    @     @ ,@@@@@
	                                   @     @
	                                  @
	`)
}

func main() { router.App.Run(iris.Addr(app.Cfg.Server.Addr()), iris.WithoutVersionChecker, iris.WithoutServerError(iris.ErrServerClosed), iris.WithOptimizations) }