// Copyright © 2017 Yo Mangan. All rights reserved.
package responses

type (
	Customer struct {
		ID	 	  int	 `json:"id"`
		Name  	  string `json:"name"`
		Address	  string `json:"address"`
		BirthDate string `json:"birth_date"`
		Gender	  string `json:"gender"`
		Phone	  string `json:"phone"`
		Email 	  string `json:"email"`
	}
	Customers []*Customer
)