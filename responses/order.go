// Copyright © 2017 Yo Mangan. All rights reserved.
package responses

type (
	Order struct {
		ID   	   int        `json:"id"`
		Invoice	   string     `json:"invoice"`
		Courier    Courier    `json:"courier"`
		Customer   Customer   `json:"customer"`
		Total 	   float64    `json:"total"`
		OrderFoods OrderFoods `json:"order_foods"`
	}
	Orders []*Order
)