// Copyright © 2017 Yo Mangan. All rights reserved.
package responses

type (
	Restaurant struct {
		ID   	int    `json:"id"`
		Name	string `json:"invoice"`
		Address string `json:"address"`
	}
	Restaurants []*Restaurant
)