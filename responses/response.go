// Copyright © 2017 Yo Mangan. All rights reserved.
package responses

type Response struct {
	Status	int	    	`json:"status"`
	Message	string	    `json:"message"`
	Data 	interface{} `json:"data,omitempty"`
}