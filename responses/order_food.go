// Copyright © 2017 Yo Mangan. All rights reserved.
package responses

type (
	OrderFood struct {
		ID   	 int     `json:"id"`
		Order    Order   `json:"order"`
		Food     Food    `json:"food"`
		Quantity int     `json:"quantity"`
		Price 	 float64 `json:"price"`
		Total 	 float64 `json:"total"`
	}
	OrderFoods []*OrderFood
)