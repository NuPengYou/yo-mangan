// Copyright © 2017 Yo Mangan. All rights reserved.
package responses

type (
	Food struct {
		ID   	   int        `json:"id"`
		Name  	   string     `json:"name"`
		Price 	   float64    `json:"price"`
		Restaurant Restaurant `json:"restaurant"`
	}
	Foods []*Food
)