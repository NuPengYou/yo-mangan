package helper

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"yo-mangan/responses"

	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
	"github.com/sentientmonkey/future"

	"golang.org/x/crypto/bcrypt"
)

type (
	Cipher struct {}
	Config struct {
		path string
		name string
	}
	ConnectionString struct {
		Host     string
		Username string
		Password string
		Database string
		Port     int
	}
	Function func(interface{}) (interface{}, error)
	FunctionWrapper struct {}
	HTTPRequest string
	MySQL struct {
		driverName string
		dataSource string
		OverSSH    bool
		Connection *ConnectionString
	}
	SSH struct {
		*MySQL
		Host     string
		Username string
		Password string
		Port     int
	}
	TokenExtractor struct {
		Type  		  string
		Authorization string
	}
)

func (c *Cipher) CompareHash(password, hash string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password)) == nil
}

func (c *Cipher) Eksblowfish(password string, cost int) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	return string(b), err
}

func (c *Config) Read(name string, value interface{}) error {
	c.path, c.name = "./json", name
	b, err := ioutil.ReadFile(filepath.Join(c.path, c.name))
	if err != nil { return fmt.Errorf("Failed to load %s config file at %s\n", c.name, err) }
	return json.Unmarshal(b, value)
}

func (fw *FunctionWrapper) Date(value string) time.Time {
	date, err := time.Parse("2006-01-02", value)
	if err != nil { panic(err) }
	return date
}

func (fw *FunctionWrapper) Error(format string, args ...interface{}) error {
	if len(args) < 1 { return errors.New(format) }
	return fmt.Errorf(format, args...)
}

func (fw *FunctionWrapper) Exec(method, text string, req, res interface{}, f Function, ctx iris.Context) error {
	if req != nil {
		if err := ctx.ReadJSON(req); err != nil { return fw.response(func () int { return iris.StatusBadRequest }, err.Error(), nil, ctx) }
	}
	if err := fw.JSON(func() interface{} {
		m := make(map[string]interface{})
		for k, v := range ctx.URLParams() { m[k] = v }
		p, err := future.NewPromise(func() (future.Value, error) { return f(m) }).Get()
		if err != nil { return err }
		for k := range m { delete(m, k) }
		return p
	}(), &res); err != nil {
		return fw.response(func() int {
			if err.Error() == "No data found" { return iris.StatusBadRequest }
			return iris.StatusInternalServerError
		}, err.Error(), nil, ctx)
	}
	status := func() int {
		if method == iris.MethodPost { return iris.StatusCreated }
		return iris.StatusOK
	}
	if method != iris.MethodGet { return fw.response(status, text, nil, ctx) }
	return fw.response(status, text, &res, ctx)
}

func (fw *FunctionWrapper) HTTPRequest(url string) (interface{}, error) {
	result, err := future.NewPromise(func() (future.Value, error) { return http.Get(url) }).Get()
	if err != nil { return err.Error(), err }
	response := result.(*http.Response)
	defer response.Body.Close()
	return ioutil.ReadAll(response.Body)
}

func (fw *FunctionWrapper) JSON(i, v interface{}) error {
	if err, ok := i.(error); ok { return err }
	b, err := json.Marshal(i)
	if err != nil { return err }
	return json.Unmarshal(b, v)
}

func (fw *FunctionWrapper) PrimaryKey(ctx iris.Context) int {
	i, err := ctx.Params().GetInt("id")
	if err != nil { fw.response(func() int { return iris.StatusBadRequest }, err.Error(), nil, ctx) }
	return i
}

// private wrapper function that return HTTP response either success or failed.
// status is function to return HTTP status code
// message is text message that informs about the request result
// data is entity data(in this case only GET method otherwise set to `nil`)
// ctx is context(iris.Context) to return HTTP response
func (fw *FunctionWrapper) response(status func() int, message string, data interface{}, ctx iris.Context) error {
	ctx.StatusCode(status())
	_, err := ctx.JSON(func() interface{} {
		if data != nil { return &responses.Response{status(), message, data} }
		return &struct {Status int `json:"status"`; Message string `json:"message"`}{status(), message}
	}())
	return err
}

func (fw *FunctionWrapper) Trace(sep string, skip int) []string {
	pc, _, _, _ := runtime.Caller(skip)
	return strings.Split(runtime.FuncForPC(pc).Name(), sep)
}

func (m *MySQL) ReadData(db *gorm.DB, f func() error, out interface{}, expr bool, params ...interface{}) error {
	var page, size int
	paging := func(key string, v interface{}, m map[string]interface{}) int {
		if i, err := strconv.Atoi(v.(string)); err != nil { return 0 } else { delete(m, key); return i }
	}
	/*RnD][15-11-2017]: Wrapper depend on expr, either filter or search with multiple parameter.
	filter := func(table *gorm.DB, query map[string]interface{}) error {
		if expr {
			for key, value := range query { err = table.Where(key + " LIKE ?", "%" + value.(string) + "%").Find(out).Error }
		} else {
			err = table.Where(query).Find(out).Error
		}
		return err
	}
	*/
	for index, param := range params {
		if query, ok := param.(map[string]interface{}); index >= 0 && ok {
			for key, value := range query {
				switch key {
				case "page":
					page = paging(key, value, query)
				case "size":
					size = paging(key, value, query)
				}
			}
		}
	}
	table := db.Limit(size).Offset(func() int {
		var count int
		if page < 1 { count = 1 } else { count = page }
		return (count - 1) * size
	}())
	err := table.Error
	/*RnD][15-11-2017]: Wrapper search with multiple parameter.
	search := func(query map[string]interface{}) error {
		for key, value := range query { err = table.Where(key + " LIKE ?", "%" + value.(string) + "%").Find(out).Error }
		return err
	}
	*/
	for index, param := range params {
		if query, ok := param.(map[string]interface{}); index >= 0 && ok {
			switch len(query) {
			case 0:
				err = table.Find(out).Error
			case 1:
				/*
				if _, ok := query["id"]; ok { err = table.First(out, query).Error } else { err = filter(table, query) }
				if _, ok := query["id"]; ok {
					err = table.First(out, query).Error
				} else {
					if expr { err = search(query) } else { err = table.Where(query).Find(out).Error }
				}
				*/
				if _, ok := query["id"]; ok {
					err = table.First(out, query).Error
				} else {
					if expr {
						for key, value := range query { err = table.Where(key + " LIKE ?", "%" + value.(string) + "%").Find(out).Error }
					} else {
						err = table.Where(query).Find(out).Error
					}
				}
			default:
				err = table.Where(query).Find(out).Error
				/*[RnD][15-11-2017]: Search with multiple parameter.
				err = filter(table, query)
				if expr { err = search(query) } else { err = table.Where(query).Find(out).Error }
				if expr {
					for key, value := range query { err = table.Where(key + " LIKE ?", "%" + value.(string) + "%").Find(out).Error }
				} else {
					err = table.Where(query).Find(out).Error
				}
				*/
			}
		}
	}
	if err != nil { return err }
	return f()
}

func (m *MySQL) Register() (db *gorm.DB, err error) {
	connection := "%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=true"
	if m.OverSSH { connection = "%s:%s@mysql+tcp(%s:%d)/%s?charset=utf8&parseTime=true" }
	m.driverName, m.dataSource = "mysql", fmt.Sprintf(connection, m.Connection.Username, m.Connection.Password, m.Connection.Host, m.Connection.Port, m.Connection.Database)
	db, err = gorm.Open(m.driverName, m.dataSource)
	if err != nil {
		fmt.Printf("Returned rows num: %s, %s", db, err)
		defer db.Close()
		return
	}
	fmt.Printf("Connected into %s database\n", m.Connection.Database)
	db.SingularTable(true)
	db.LogMode(true)
	return
}

func (m *MySQL) Transact(db *gorm.DB, f func() error) error {
	// begin transaction
	tx := db.Begin()
	if err := tx.Error; err != nil {
		tx.Rollback()
		return err
	}
	// do some operations in the transaction
	err := f()
	if err != nil { return err }
	tx.Commit()
	return err
}

func (s *SSH) Connect() *gorm.DB {
	db, _ := s.Register()
	return db
}